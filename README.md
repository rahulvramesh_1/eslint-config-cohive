# Cohive ES Linter

This package extends Airbnb and provides .eslintrc as an **extensible shared config**.

How to install:
```bash
 yarn add -D git+ssh:git@bitbucket.org:evhivetech/eslint-config-cohive.git
```

In your .eslintrc file: 
```
{
  extends: "eslint-config-cohive"
}
```
